﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileDownloader
{
    public partial class MainWindow : Window
    {
        string url = null;
        string path = null;
        WebClient wc = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            url = urlTextbox.Text;
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();

            var dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == System.Windows.Forms.DialogResult.OK)
            {
                path = openFileDialog.SelectedPath;
            }
        }

        private void Download_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (wc = new WebClient())
                {
                    wc.DownloadProgressChanged += ProgressBar_ValueChanged;
                    if (path == null || url == null)
                    {
                        throw new ArgumentNullException();
                    }
                    wc.DownloadFileAsync(new Uri(url), path + "\\" + System.IO.Path.GetFileName(url));
                }
            }
            catch (ArgumentNullException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            catch (UriFormatException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            catch (WebException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ProgressBar_ValueChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            progressBar.Value = 0;
            wc.CancelAsync();
        }
    }
}
