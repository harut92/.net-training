﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome
{
    static class StringHelper
    {
        public static bool IsPalindrome(this string str)
        {
            if (str == null)
            {
                throw new ArgumentNullException();
            }

            str.ToUpper();

            for (int i = 0; i < str.Length / 2; i++)
            {
                if (str[i] != str[str.Length - 1 - i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
