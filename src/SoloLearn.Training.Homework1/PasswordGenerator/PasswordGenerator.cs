﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordGenerator
{
    class PasswordGenerator
    {
        public int Length { get;private set; }
        private bool RequireLowercase { get; set; }
        private bool RequireUppercase { get; set; }
        private bool RequireDigit { get; set; }
        private bool RequireSymbols { get; set; }
        private string Charcaters { get; set; }

        public PasswordGenerator()
        {
            Length = 16;
            RequireLowercase = true;
            RequireUppercase = true;
        }

        public PasswordGenerator(int length)
        {
            if (length <= 0)
            {
                Length = 16;
            }
            else
            {                 
                Length = length;
            }
            RequireLowercase = true;
            RequireUppercase = true;
        }

        private string RequiredChars()
        {
            string[] charStrings = new string[] {
            "ABCDEFGHJKLMNOPQRSTUVWXYZ",
            "abcdefghijkmnopqrstuvwxyz",
            "0123456789",               
            "!@$?_-"                    
            };

            if (!RequireLowercase && !RequireUppercase && !RequireDigit && !RequireSymbols)
            {
                RequireUppercase = true;
                RequireLowercase = true;
            }

            string chars=string.Empty;
            if (RequireLowercase)
            {
                chars += charStrings[0];
            }

            if (RequireUppercase)
            {
                chars += charStrings[1];
            }

            if (RequireDigit)
            {
                chars += charStrings[2];
            }

            if (RequireSymbols)
            {
                chars += charStrings[3];
            }

            return chars;
        }

        public string GeneratePassword()
        {
            Charcaters = RequiredChars();
            string password = string.Empty;
            Random rnd = new Random();
            StringBuilder str = new StringBuilder(Length);

            for (int i = 0; i < Length; i++)
            {
                str.Append(Charcaters[rnd.Next(0, Charcaters.Length)]);
            }

            return str.ToString();
        }

        public string GeneratePassword(bool requireLowercase,bool requireUppercase, bool requireDigit, bool requireSymbols)
        {
            RequireLowercase = requireLowercase;
            RequireUppercase = requireUppercase;
            RequireDigit = requireDigit;
            RequireSymbols = requireSymbols;
            Charcaters = RequiredChars();
            string password = string.Empty;
            Random rnd = new Random();
            StringBuilder str = new StringBuilder(Length);
            for (int i = 0; i < Length; i++)
            {
                str.Append(Charcaters[rnd.Next(0, Charcaters.Length)]);
            }

            return str.ToString();
        }
    }
}
