﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinValue
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                double[] arr = { 123.5, 342, 34, 65, 2, 234, -5.8, 4 };
                Console.WriteLine(arr.MinVal());
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
