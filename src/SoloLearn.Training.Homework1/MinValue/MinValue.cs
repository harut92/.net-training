﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinValue
{
    static class MinValue
    {
        public static T MinVal<T>(this T[] array) where T : IComparable<T>
        {
            if (array == null)
            {
                throw new ArgumentNullException();
            }

            if (array.Length == 0)
            {
                throw new ArgumentException();
            }

            T minValue = array[0];

            foreach (T item in array)
            {
                if (item.CompareTo(minValue) < 0)
                {
                    minValue = item;
                }
            }

            return minValue;
        }

    }
}
